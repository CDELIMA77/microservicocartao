package br.com.mastertech.cartaoserv.repositories;

import br.com.mastertech.cartaoserv.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Long> {
    Optional<Cartao> findByNumero(String numero);
}