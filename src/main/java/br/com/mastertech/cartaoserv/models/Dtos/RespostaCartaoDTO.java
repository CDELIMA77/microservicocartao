package br.com.mastertech.cartaoserv.models.Dtos;

import br.com.mastertech.cartaoserv.client.ClienteClient;

public class RespostaCartaoDTO {
    private Long id;
    private String numero;
    private Long clienteId;
    private String clienteNome;
    boolean ativo;

    public RespostaCartaoDTO() {
    }

    public RespostaCartaoDTO(Long id, String numero, Long clienteId, String clienteNome, boolean ativo) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
        this.clienteNome = clienteNome;
        this.ativo = ativo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }

    public String getClienteNome() {
        return clienteNome;
    }

    public void setClienteNome(String clienteNome) {
        this.clienteNome = clienteNome;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}

