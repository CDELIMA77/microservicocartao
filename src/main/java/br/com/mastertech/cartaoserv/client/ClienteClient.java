package br.com.mastertech.cartaoserv.client;

import br.com.mastertech.cartaoserv.models.Dtos.ClienteDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {
    @GetMapping("/cliente/{id}")
    ClienteDTO getClienteById(@PathVariable Long id);
}
