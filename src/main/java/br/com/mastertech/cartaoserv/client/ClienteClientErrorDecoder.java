package br.com.mastertech.cartaoserv.client;

import br.com.mastertech.cartaoserv.exceptions.ManipuladorDeExcecoes;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response){
        if (response.status() == 400) { return new ManipuladorDeExcecoes.ClienteNotFoundException(); }
        else   {return errorDecoder.decode(s, response);}
    }
}
