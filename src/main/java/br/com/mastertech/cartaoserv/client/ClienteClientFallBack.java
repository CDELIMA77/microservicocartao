package br.com.mastertech.cartaoserv.client;

import br.com.mastertech.cartaoserv.exceptions.ManipuladorDeExcecoes;
import br.com.mastertech.cartaoserv.models.Dtos.ClienteDTO;
import com.netflix.client.ClientException;

import java.io.IOException;
import java.net.ConnectException;

public class ClienteClientFallBack implements ClienteClient {
       private Exception cause;

       ClienteClientFallBack(Exception cause) {
           this.cause = cause;
       }

       @Override
       public ClienteDTO getClienteById(Long id){
           System.out.println(cause.getLocalizedMessage());
           System.out.println(cause.getClass().getName());
           /* Nao colocar o Runtime aqui pois para de funcionar o ErrorDecoder */
           if ( cause instanceof IOException || cause instanceof ConnectException || ( cause.getLocalizedMessage() != null && cause.getLocalizedMessage().contains("ClientException") )) {
               /* Para aparecer no Postman */
               throw new ManipuladorDeExcecoes.ClienteOffLineException();
               /* se quiser apenas logar no console
               throw new RuntimeException("Serviço cliente fora do ar");*/
           }


           throw (RuntimeException) cause;


           /* se quiser enviar mais dados
           ClienteDTO clienteDTO = new ClienteDTO();
           clienteDTO.setId(-1L);
           clienteDTO.setNome("Joao ninguém");
           return clienteDTO; /* até aqui opcional
           /* return new ClienteDTO(); */
       }

}
