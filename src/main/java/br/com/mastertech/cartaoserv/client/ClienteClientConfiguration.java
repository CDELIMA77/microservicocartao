package br.com.mastertech.cartaoserv.client;

import feign.Feign;
import feign.FeignException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder(){
        return new ClienteClientErrorDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallbackFactory(ClienteClientFallBack::new)
                .build();
        return Resilience4jFeign.builder(decorators);

    }

}
