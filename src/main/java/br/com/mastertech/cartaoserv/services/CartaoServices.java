package br.com.mastertech.cartaoserv.services;

import br.com.mastertech.cartaoserv.client.ClienteClient;
import br.com.mastertech.cartaoserv.exceptions.ManipuladorDeExcecoes;
import br.com.mastertech.cartaoserv.models.Cartao;
import br.com.mastertech.cartaoserv.models.Dtos.CartaoDTO;
import br.com.mastertech.cartaoserv.models.Dtos.ClienteDTO;
import br.com.mastertech.cartaoserv.models.Dtos.RespostaCartaoDTO;
import br.com.mastertech.cartaoserv.repositories.CartaoRepository;
import feign.FeignException;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CartaoServices {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Iterable<Cartao> buscarTodosCartoes(List<Long> cartaoId) {
        Iterable<Cartao> cartaoIterable = cartaoRepository.findAllById(cartaoId);
        return cartaoIterable;
    }

    public Iterable<Cartao> buscarTodosCartoes() {
        Iterable<Cartao> cartao = cartaoRepository.findAll();
        return cartao;
    }

    public Optional<Cartao> buscarPorId(Long id) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        return cartaoOptional;
    }

    public Optional<Cartao> buscarPorNumero(String numero) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        return cartaoOptional;
    }

    public RespostaCartaoDTO salvarCartao(CartaoDTO cartaoDTO) throws ObjectNotFoundException {
        RespostaCartaoDTO respostaCartao = new RespostaCartaoDTO();
        Cartao cartao = new Cartao();
        ClienteDTO clienteDTO;
        /*como programou o decoder não precisa do try catch
        try {*/
            clienteDTO = clienteClient.getClienteById(cartaoDTO.getClienteId());
        System.out.println("Buscaram um cliente de id" + cartaoDTO.getClienteId() + "em" + System.currentTimeMillis());
                cartao.setNumero(cartaoDTO.getNumero());
                cartao.setCliente_id(clienteDTO.getId());
                cartao.setAtivo(false);

                Cartao cartaoObjeto = cartaoRepository.save(cartao);

                respostaCartao.setId(cartaoObjeto.getId());
                respostaCartao.setNumero(cartaoObjeto.getNumero());
                respostaCartao.setClienteId(cartaoObjeto.getCliente_id());
                respostaCartao.setClienteNome(clienteDTO.getNome());
                respostaCartao.setAtivo(cartaoObjeto.isAtivo());
                return respostaCartao;
        /*}
        catch (FeignException.BadRequest e) {
                 throw new ManipuladorDeExcecoes.ClienteNotFoundException();
        }*/
    }

    public RespostaCartaoDTO atualizarCartao(Cartao cartao) throws ObjectNotFoundException {
        Optional<Cartao> cartaoOptional = buscarPorNumero(cartao.getNumero());
        ClienteDTO clienteDTO;
        if (cartaoOptional.isPresent()) {
            RespostaCartaoDTO respostaCartao = new RespostaCartaoDTO();
            Cartao cartaoData = cartaoOptional.get();

            if (cartao.isAtivo()) { cartaoData.setAtivo(cartao.isAtivo()); } else {cartaoData.setAtivo(false);}

            if (cartao.getNumero() != null) {
                cartaoData.setNumero(cartao.getNumero());
            }

            if ( cartao.getCliente_id() !=  null  )  {cartaoData.setCliente_id(cartao.getCliente_id());}

            try {   clienteDTO = clienteClient.getClienteById(cartaoData.getCliente_id());
                    respostaCartao.setClienteNome(clienteDTO.getNome());
                }
            catch (FeignException.BadRequest e) {
                    throw new ManipuladorDeExcecoes.ClienteNotFoundException();
                }

            Cartao cartaoObjeto = cartaoRepository.save(cartaoData);

            respostaCartao.setId(cartaoObjeto.getId());
            respostaCartao.setNumero(cartaoObjeto.getNumero());
            respostaCartao.setClienteId(cartaoObjeto.getCliente_id());
            if (cartaoData.isAtivo()) {respostaCartao.setAtivo(true);} else {respostaCartao.setAtivo(false);}

            return respostaCartao;
        }
        throw new ObjectNotFoundException(Cartao.class, "Cartao não cadastrado");
    }

    public void deletarCartao(Cartao cartao) {
        cartaoRepository.delete(cartao);
    }
}