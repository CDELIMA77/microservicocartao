package br.com.mastertech.cartaoserv.exceptions;

import br.com.mastertech.cartaoserv.errors.ObjetoDeErro;
import br.com.mastertech.cartaoserv.errors.RespostaDeErro;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ManipuladorDeExcecoes extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        List<ObjetoDeErro> objetoDeErros = getErros(ex);
        RespostaDeErro respostaDeErro = getRespostaDeErro(ex, status, objetoDeErros);
        return ResponseEntity.status(status).body(respostaDeErro);

    }

    private RespostaDeErro getRespostaDeErro(MethodArgumentNotValidException notvalid, HttpStatus status, List<ObjetoDeErro>  objetoDeErros ) {
        RespostaDeErro respostaDeErro = new RespostaDeErro("Erro de validação", status.value(), status.getReasonPhrase(), notvalid.getBindingResult().getObjectName(), objetoDeErros);
        return respostaDeErro;
    }


    private List<ObjetoDeErro> getErros(MethodArgumentNotValidException exception){
        List<ObjetoDeErro> objetoDeErros = exception.getBindingResult().getFieldErrors().stream().map(erro -> new ObjetoDeErro(erro.getDefaultMessage(), erro.getField(), erro.getRejectedValue())).collect(Collectors.toList());
        return objetoDeErros;
    }


    @ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O Cliente não existe")
    public static class ClienteNotFoundException extends RuntimeException{}

    @ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Serviço Cliente fora do ar")
    public static class ClienteOffLineException extends RuntimeException{}


}
