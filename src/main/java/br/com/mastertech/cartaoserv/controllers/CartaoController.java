package br.com.mastertech.cartaoserv.controllers;

import br.com.mastertech.cartaoserv.client.ClienteClient;
import br.com.mastertech.cartaoserv.models.Cartao;
import br.com.mastertech.cartaoserv.models.Dtos.*;
import br.com.mastertech.cartaoserv.services.CartaoServices;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoServices cartaoServices;

    @Autowired
    private ClienteClient clienteClient;

    @GetMapping
    public Iterable<Cartao> buscarTodosCartoes(){
        return cartaoServices.buscarTodosCartoes();
    }

    /* Quando nao vai usar e somente quer chamar a rota de outro microserviço
    @GetMapping("/cliente/{id}")
    public ClienteDTO getCliente(@PathVariable Long id) {
        return clienteClient.getClienteById(id);
    }*/

    @GetMapping("/{numero}")
    public ResponseEntity<RespostaCartaoGet> buscarCartao(@PathVariable String numero){
        Optional<Cartao> cartaoOptional = cartaoServices.buscarPorNumero(numero);
        if (cartaoOptional.isPresent()){
            RespostaCartaoGet respostaGET = new RespostaCartaoGet();
            respostaGET.setId(cartaoOptional.get().getId());
            respostaGET.setNumero(cartaoOptional.get().getNumero());
            respostaGET.setClienteId(cartaoOptional.get().getCliente_id());
            ClienteDTO clienteDTO = clienteClient.getClienteById(respostaGET.getClienteId());
            return ResponseEntity.status(200).body(respostaGET);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Optional<Cartao>> buscarCartao(@PathVariable Long id){
        Optional<Cartao> cartaoOptional = cartaoServices.buscarPorId(id);
        if (cartaoOptional.isPresent()){
            return ResponseEntity.status(200).body(cartaoOptional);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<RespostaCartaoDTO> incluirCartao(@RequestBody @Valid CartaoDTO cartaoDTO) {
        RespostaCartaoDTO cartaoObjeto;
        try { cartaoObjeto = cartaoServices.salvarCartao(cartaoDTO); }
        catch (ObjectNotFoundException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return ResponseEntity.status(201).body(cartaoObjeto);
    }

    @PutMapping("/{numero}")
    public ResponseEntity<RespostaCartaoDTO> atualizarCartao(@PathVariable String numero, @RequestBody @Valid Cartao cartao){
        cartao.setNumero(numero);
        RespostaCartaoDTO respostaDTO;
        try{ respostaDTO = cartaoServices.atualizarCartao(cartao);}
        catch (ObjectNotFoundException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return ResponseEntity.status(200).body(respostaDTO);
    }

    @PatchMapping("/{numero}")
    public ResponseEntity<RespostaCartaoDTO> ativarCartao(@PathVariable String numero, @RequestBody UpdateCartaoRequest updateCartaoRequest ){
        Cartao cartao = new Cartao();
        cartao.setNumero(numero);
        cartao.setAtivo(updateCartaoRequest.getAtivo());
        RespostaCartaoDTO respostaDTO;
        try{ respostaDTO = cartaoServices.atualizarCartao(cartao); }
        catch (ObjectNotFoundException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return ResponseEntity.status(200).body(respostaDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Cartao> deletarCartao(@PathVariable Long id){
        Optional<Cartao> cartaoOptional = cartaoServices.buscarPorId(id);
        if (cartaoOptional.isPresent()) {
            cartaoServices.deletarCartao(cartaoOptional.get());
            return ResponseEntity.status(204).body(cartaoOptional.get());
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
}

